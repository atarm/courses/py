# Multi Environments

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

## Overview

>1. <https://stackoverflow.com/a/41573588/>

1. `Python standard library`:
    1. `pyvenv`
        1. **_[deprecated_ since 3.6](https://docs.python.org/dev/whatsnew/3.6.html#id8)_**
    1. `venv`
        1. 替代`pyvenv`，从`Python 3.3`开始支持`venv`，描述于["PEP 405 -- Python Virtual Environments"](https://www.python.org/dev/peps/pep-0405/)
        1. 实现与`virtualenv`相似的功能（`virtualenv`特征的子集），[Compatibility with the stdlib venv module](https://virtualenv.pypa.io/en/16.7.9/reference.html#compatibility-with-the-stdlib-venv-module)
1. `PyPi library`:
    1. [`virtualenv`](https://pypi.org/project/virtualenv/)： 解决同一个`Python`版本下不同的`project`环境的问题
        1. `virtualenvwrapper`
    1. `pipenv`：
        1. aims to combine `Pipfile`, `pip` and `virtualenv` into one command on the command-line.
        1. `pipenv`使用`Pipfile`代替`requirements.txt`管理依赖包及其依赖关系
1. `third-part application`:
    1. `pyenv`： 解决同一台机器下不同的`Python`版本环境问题
        1. `pyenv-virtualenv`
        1. `pyenv-virtualenvwrapper`

## Problems We Meet

怎样有效地管理不同项目对依赖的不同需求
