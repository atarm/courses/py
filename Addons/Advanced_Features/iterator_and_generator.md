# Iterator and Generator迭代器与生成器

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [Iterator and Iterable迭代器与可迭代](#iterator-and-iterable迭代器与可迭代)
2. [Generator生成器](#generator生成器)
3. [Bibliographies](#bibliographies)

<!-- /code_chunk_output -->

## Iterator and Iterable迭代器与可迭代

## Generator生成器

## Bibliographies

1. :star2: Iterables vs. Iterators vs. Generators: <https://nvie.com/posts/iterators-vs-generators/>
1. Python中生成器和迭代器的区别: <https://blog.csdn.net/u014745194/article/details/70176117>
