---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Tkinter_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Tkinter

---

## 提纲Outline

1. :star2: `Tkinter`概述
1. `Tkinter`常用组件
1. `Tkinter`编程的一般步骤

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Tkinter`概述

---

1. `tkinter`（Tk Interface）： 对`Tcl/Tk`的封装，`Tk`是一个支持多个`OS`的图形库使用`Tcl`语言开发
1. `wxPython`： 对`wxWidgets(C++)`的封装
1. `PyGTK`： 对`GTK+ 2(C)`的封装
1. `PyGObject`： 对`GTK+ 3(C)`的封装
1. `PyQt`： 对`Qt(C++)`的封装

><https://wiki.python.org/moin/GuiProgramming>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `Tkinter`常用组件

><https://sunhwee.com/posts/80fa3a85.html>

---

1. `Tk`： 应用程序顶层窗口
1. `Toplevel`： 顶层窗口
1. `Frame`： 框架，其他组件的容器，常用于组件分组
1. `Button`： 按键
1. `Radiobutton`： 单选框/单选按钮，同组至多一个处于选中状态
1. `Checkbutton`： 复选框形式的按钮

---

1. `Menu`： 菜单
1. `Label`： 标签，常用来显示单行文本
1. `Entry`： 单行文本框
1. `Message`： 多行文本框
1. `Listbox`： 列表框
1. `Scrollbar`： 滚动条
1. `Canvas`： 画布，用于绘制直线、椭圆、多边形等各种图形

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## `TKinter`编程的一般步骤

---

1. 创建并设置主窗口容器
1. 创建并设置组件、将组件放置到容器中
1. 设置消息处理函数
1. 启动主窗口容器的消息循环

---

```python {.line-numbers}
import tkinter

root = tkinter.Tk()

coding_langs = ['C', 'python', 'php', 'html', 'SQL', 'java']
movies = ['CSS', 'jQuery', 'Bootstrap']
listbox_01 = tkinter.Listbox(root)  # 创建两个列表组件
listbox_02 = tkinter.Listbox(root)

for item in coding_langs:  # 第一个小部件插入数据
    listbox_01.insert(0, item)

for item in movies:  # 第二个小部件插入数据
    listbox_02.insert(0, item)

listbox_01.pack()  # 将小部件放置到主窗口中
listbox_02.pack()

root.mainloop()  # 进入消息循环
```

---

```python {.line-numbers}
import tkinter as tk

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.hi_there = tk.Button(self)
        self.hi_there["text"] = "Hello World\n(click me)"
        self.hi_there["command"] = self.say_hi
        self.hi_there.pack(side="top")

        self.quit = tk.Button(self, text="QUIT", fg="red",
                              command=self.master.destroy)
        self.quit.pack(side="bottom")

    def say_hi(self):
        print("hi there, everyone!")

root = tk.Tk()
app = Application(master=root)
app.mainloop()
```

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok:End of This Slide:ok:
