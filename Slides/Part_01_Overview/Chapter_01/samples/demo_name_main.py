from demo_name_module import my_function

def main():
    print("in" + __file__)
    print("变量 __name__ 的值是 " + __name__)
    my_function()

if __name__ == "__main__":
    main()
else:
    print("this module is not called directly...")