# PY-E04：Python面向对象程序设计

## 实验基本信息

1. 实验性质：设计性
1. 实验学时：4
1. 实验目的：
    1. 掌握`Python`类的设计与实现
    1. 掌握`Python`异常捕获
1. 实验内容与要求：
    1. 实验内容01： `双端队列deque`
    1. 实验内容02： `栈stack`
    1. 实验内容03： 捕获多异常
1. 实验条件：
    1. 硬件环境： `PC`
    1. 软件环境： `IDLE`或`Jupyter`或`PyCharm`

## 实验注意事项

1. 实验指引中使用尖括号`<>`表示键盘按键，如：`<CTRL>`代表control键，`<j>`代表j键
1. 实验中请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”
1. 实验内容与实验指引中各子内容包含三部分：
    1. **:microscope: 实验内容具体任务** ==> 实验的具体任务及步骤
    1. **:ticket: 实验内容参考截图** ==> 实验的参考结果截图
    1. **:bookmark: 实验内容相关知识** ==> 实验涉及的相关知识

## 实验前提条件与储备知识

## 实验指引

### 实验内容01

#### :microscope: 实验内容01具体任务

1. 程序目标： 设计`双端队列deque`，实现入队、出队等基本队列操作
1. 程序要求：
    1. 在程序运行开始输出两行文本，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning: 请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
        1. `And now... It is PY-E04 content 01...`
    1. `双端队列deque`和演示使用写在同一个`py`文件中即可

#### :ticket: 实验内容01参考截图

![](./assets_image/E0401_code_01.png)

![](./assets_image/E0401_code_02.png)

![](./assets_image/E0401_code_03.png)

#### :bookmark: 实验内容01相关知识

### 实验内容02

#### :microscope: 实验内容02具体任务

1. 程序目标： 设计`栈stack`，实现入栈、出栈等基本栈操作
1. 程序要求：
    1. 在程序运行开始输出两行文本，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning: 请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
        1. `And now... It is PY-E04 content 02...`
    1. `栈stack`和演示使用写在同一个`py`文件中即可

#### :ticket: 实验内容02参考截图

![](./assets_image/E0402_code_01.png)

![](./assets_image/E0402_code_02.png)

#### :bookmark: 实验内容02相关知识

### 实验内容03

#### :microscope: 实验内容03具体任务

1. 程序目标： 设计一个除法函数，该函数能捕获多种类型的异常
1. 程序要求：
    1. 在程序运行开始输出两行文本，分别如下：
        1. `I am EN_and_FN_in_PY... I am learning hard in Python...`（:warning: 请将`EN_and_FN_in_PY`改成您的学号和全名的拼音，如“68zhangsan”）
        1. `And now... It is PY-E04 content 03...`
    1. 除法函数接受两个`str`形参，返回代表该除法运算及其结果的`str`

#### :ticket: 实验内容03参考截图

![](./assets_image/E0403_code.png)

#### :bookmark: 实验内容03相关知识

## 延伸阅读
